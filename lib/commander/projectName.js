var program = require('commander');


module.exports = () => {
  var PROJECT_NAME
  program
    .command('init <Project_Name>')
    .option('-f, --fast', 'Remove recursively')
    .action(function (dir, cmd) {
      //  console.log('remove ' + dir + (cmd.fast ? ' recursively' : ''))
      PROJECT_NAME =  dir
    })
  
  program.parse(process.argv)


  return PROJECT_NAME ? PROJECT_NAME : null
}