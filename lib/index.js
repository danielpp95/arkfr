const shell = require("shelljs");

const askQuestions = require('./askQuestions')

const init = require('./init')

const createFolders = require('./createFolders')

const createFiles = require('./createFiles')

const addTextToFiles = require('./files/index')

const init_npm = require('./init_npm')

const success = require('./success')

const PROJECT_NAME = require('./commander/projectName')()

const addMiddlewares = require('./middelwares/middlewares')

const run = async () => {
  // show script introduction
  // init();

  // ask questions
  const answers = await askQuestions();
  const { VIEW_ENGINE, DATABASE, MIDDLEWARES } = answers;

  // create the folders
  createFolders(PROJECT_NAME, VIEW_ENGINE, DATABASE);
  
  // create the files
  createFiles(PROJECT_NAME, VIEW_ENGINE, DATABASE);
  addTextToFiles(PROJECT_NAME, VIEW_ENGINE, DATABASE, MIDDLEWARES)

  // add middlewares
  addMiddlewares(PROJECT_NAME, VIEW_ENGINE, DATABASE, MIDDLEWARES)

  // init npm and install dependencyes
  init_npm(PROJECT_NAME, VIEW_ENGINE, DATABASE, MIDDLEWARES);

  // show success message
  success(`${PROJECT_NAME} created successfully`);
};

function error() {
  console.log("Error: invalid request");
}
PROJECT_NAME ? run() : error()