const shell = require("shelljs");
module.exports = (PROJECT_NAME, VIEW_ENGINE, DATABASE, MIDDLEWARES)  => {
  shell.cd(`${PROJECT_NAME}`)
  shell.exec('npm init --yes')
  shell.exec('npm i -S express')
  shell.exec('npm i -D nodemon typescript ts-node morgan @types/express')
  
  if (VIEW_ENGINE === "handlebars") {
    shell.exec('npm i -S express-handlebars')
    shell.exec('npm i -D @types/express-handlebars')
  }
  if(DATABASE ==="mongoose"){
    shell.exec('npm i -S mongoose')
    shell.exec('npm i -D @types/mongoose')
  }

  if(MIDDLEWARES.includes('morgan')) {
    shell.exec('npm i -S morgan')
    shell.exec('npm i -D @types/morgan')
  }

  if(MIDDLEWARES.includes('passport')) {
    shell.exec('npm i bcryptjs passport passport-local express-session')
    shell.exec('npm i -D @types/bcryptjs @types/passport')
  }

  shell.cd('../')
}