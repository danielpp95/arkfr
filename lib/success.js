const chalk = require("chalk");
module.exports = (msg) => {
  console.log(
    chalk.white.bgGreen.bold(`${msg}`)
  );
};