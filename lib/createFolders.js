const shell = require("shelljs");
module.exports = (PROJECT_NAME, VIEW_ENGINE, DATABASE) => {
  shell.mkdir(PROJECT_NAME);
  shell.mkdir(PROJECT_NAME + '/src');
  shell.mkdir(PROJECT_NAME + '/src/api');
  shell.mkdir(PROJECT_NAME + '/src/api/controllers');
  shell.mkdir(PROJECT_NAME + '/src/api/interfaces');
  shell.mkdir(PROJECT_NAME + '/src/api/models');
  shell.mkdir(PROJECT_NAME + '/src/config');
  shell.mkdir(PROJECT_NAME + '/src/helpers');
  shell.mkdir(PROJECT_NAME + '/src/helpers/arcfr');
  shell.mkdir(PROJECT_NAME + '/src/public');
  shell.mkdir(PROJECT_NAME + '/src/server');
  shell.mkdir(PROJECT_NAME + '/src/views');
  
  if(VIEW_ENGINE === 'handlebars') {
    shell.mkdir(PROJECT_NAME + '/src/views/layouts');
    shell.mkdir(PROJECT_NAME + '/src/views/partials');
  }
};