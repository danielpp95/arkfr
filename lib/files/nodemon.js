const fs = require('fs')
const path = require('path')

module.exports = (PROJECT_NAME) => {
  var file = ''
  file += code(PROJECT_NAME)

  const p = (`${PROJECT_NAME}/nodemon.json`)

  fs.writeFileSync(p, file, (err) => {
    if (err) throw err;
  });
}

// #region CODE
const code = (PROJECT_NAME) => `{
  "watch": [
    "src"
  ],
  "ext": "ts",
  "ignore": [
    "src/**/*.spec.ts"
  ],
  "exec": "ts-node ./src/app.ts"
}`
// #endregion
