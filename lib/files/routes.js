const fs = require('fs')
const path = require('path')

module.exports = (PROJECT_NAME) => {
  var file = ''
  file += code(PROJECT_NAME)

  const p = (`${PROJECT_NAME}/src/server/routes.ts`)

  fs.writeFileSync(p, file, (err) => {
    if (err) throw err;
  });
}

// #region CODE
const code = (PROJECT_NAME) => `import IRoute from '../api/interfaces/IRoute'
import { Router } from "express";
var router = Router();

const routes: IRoute[] = [
]

routes.forEach(r => {
  const { method, route, controller } = r
  const fn = require('../helpers/arcfr/route')(controller)

  method == 'GET' && router.get(route, fn);
  method == 'POST' && router.post(route, fn);
  method == 'PUT' && router.put(route, fn);
  method == 'PATCH' && router.patch(route, fn);
  method == 'DELETE' && router.delete(route, fn);
});

module.exports= router;`
// #endregion
