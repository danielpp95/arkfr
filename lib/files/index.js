const app_ts = require('./app')
const mongoose = require('./database_mongoose')
const nodemon = require('./nodemon')
const readme = require('./readme')
const tsconfig = require('./tsconfig')
const docker_compose = require('./docker_compose')
const routes = require('./routes')
const helpers = require('./helpers')
const iroute = require('./iroute')
// const gitignore = require('./gitignore')

module.exports = (PROJECT_NAME, VIEW_ENGINE, DATABASE, MIDDLEWARES) => {
  
  app_ts(PROJECT_NAME, VIEW_ENGINE, DATABASE, MIDDLEWARES);
  if (DATABASE === 'mongoose') { mongoose(PROJECT_NAME) }
  // gitignore();
  nodemon(PROJECT_NAME);
  readme(PROJECT_NAME);
  tsconfig(PROJECT_NAME);
  docker_compose(PROJECT_NAME);
  routes(PROJECT_NAME);
  helpers(PROJECT_NAME);
  iroute(PROJECT_NAME);
}