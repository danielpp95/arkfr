const fs = require('fs')
const path = require('path')

module.exports = (PROJECT_NAME) => {
  var file = ''
  file += code(PROJECT_NAME)

  const p = (`${PROJECT_NAME}/docek-compose.yml`)

  fs.writeFileSync(p, file, (err) => {
    if (err) throw err;
  });
}

// #region CODE
const code = (PROJECT_NAME) => `
version: "3.5"
services:

  mongo:
    container_name: mongo
    image: mongo
    ports:
      - "27017:27017"
`
// #endregion
