const fs = require('fs')
const path = require('path')

// #region Imports
const imports = 
`import express from 'express';
import path from 'path';`

const import_morgan = `
import morgan from 'morgan';`

const import_passport = `
const session = require('express-session');
const passport = require('passport')`

  const import_handlebars = `
  import exphbs from 'express-handlebars';`
// #endregion

// #region initializations
const initializations = `

const app = express();
import './config/database';
`
init_passport = `
import './config/passport';`
// #endregion

// #region settings
const settings = `
app.set('port', process.env.PORT || 1610);
`
// #endregion

// #region middlewares
const middleware_morgan = `
app.use(morgan('dev'));
`
const middleware_passport = `
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}));
app.use(passport.initialize())
app.use(passport.session())`

const middlewares = `
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.set('views', path.join(__dirname, 'views'));
`
// #endregion

// #region view engine
const handlebars = `
app.engine('.hbs', exphbs({
  extname: '.hbs',
  defaultLayout: 'main',
  layoutsDir: path.join(app.get('views'), 'layouts'),
  partialsDir: path.join(app.get('views'), 'partials')
}));
app.set('view engine', '.hbs');
`
// #endregion

// #region global variables
const globalVar_passport = `
res.locals.user = req.user || null;`
// endregion

const routes = `
app.use(require('./server/routes'))
`
const staticFiles = `
app.use(express.static(path.join(__dirname, 'public')));
`

const serverListen = "app.listen(app.get('port'), () => {\n console.log(`Server onport ${app.get('port')}`); \n})"
// #endregion

module.exports = (PROJECT_NAME, VIEW_ENGINE, DATABASE, MIDDLEWARES) => {
  var file = ''

  // imports
  file+=(imports)
  if(VIEW_ENGINE == 'handlebars') { file+=(import_handlebars)}
  if(MIDDLEWARES.includes('morgan')) { file+=(import_morgan)}
  if(MIDDLEWARES.includes('passport')) { file+=(import_passport)}
  
  // initializations
  file+=(initializations)
  if(MIDDLEWARES.includes('passport')) { file+=(init_passport)}
  
  // settings
  file+=(settings)
  
  // middlewares
  file+=(middlewares)
  if(MIDDLEWARES.includes('morgan')) {file+=middleware_morgan}
  if(MIDDLEWARES.includes('passport')) { file+=(middleware_passport)}
  
  // view engine
  if(VIEW_ENGINE == 'handlebars') { file+=(handlebars)}
  
  // global variables
  file+= 'app.use((req, res, next) => {'
  
  if(MIDDLEWARES.includes('passport')) { file+=(globalVar_passport)}
  file+= `  next();
})`

  
  // routes
  file+=(routes)

  file+=(staticFiles)
  file+=(serverListen)

  const p = path.join(`${PROJECT_NAME}/src/app.ts`)

  fs.writeFileSync(p, file, (err) => {
    if (err) throw err;
  });
}