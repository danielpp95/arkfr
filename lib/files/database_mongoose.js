const fs = require('fs')
const path = require('path')

// #region CODE
const code = (PROJECT_NAME) => `import mongoose from 'mongoose'

mongoose.connect('mongodb://localhost/${PROJECT_NAME}',  {
useNewUrlParser: true
})
.then(db => console.log('DB is connected'))
.catch(err => console.log(err))`
// #endregion

module.exports = (PROJECT_NAME) => {
  var file = ''
  file += code(PROJECT_NAME)

  const p = (`${PROJECT_NAME}/src/config/database.ts`)

  fs.writeFileSync(p, file, (err) => {
    if (err) throw err;
  });
}