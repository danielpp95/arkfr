const fs = require('fs')
const path = require('path')

module.exports = (PROJECT_NAME) => {
  var file = ''
  file += code(PROJECT_NAME)

  const p = (`${PROJECT_NAME}/src/api/interfaces/IRoute.ts`)

  fs.writeFileSync(p, file, (err) => {
    if (err) throw err;
  });
}

// #region CODE
const code = (PROJECT_NAME) => `export default interface IRoute {
  "method": 'GET' | 'POST' | 'PUT' |'PATCH' | 'DELETE',
  "route": string,
  "controller": string
}`
// #endregion
