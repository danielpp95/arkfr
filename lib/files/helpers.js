const fs = require('fs')
const path = require('path')

module.exports = (PROJECT_NAME) => {
  var file = ''
  file += code(PROJECT_NAME)

  const p = (`${PROJECT_NAME}/src/helpers/arcfr/route.ts`)

  fs.writeFileSync(p, file, (err) => {
    if (err) throw err;
  });
}

// #region CODE
const code = (PROJECT_NAME) => `const path = require('path');
const controllers = path.join(__dirname, '../../api/controllers')

module.exports = (controller: string)=> {
  var p = ''

  try {
    p = path.join(controllers, controller)
  } catch (error) {
    console.log(\`Cannot join routes $\{controllers}, $\{controller}\`);
  }
  
  var method: void;
  try {
    method = require(p).fn;
  } catch (error) {
    console.log(\`ERROR: Cannot get file $\{controller}\`)
  }

  return method;
}`
// #endregion
