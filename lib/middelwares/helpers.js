const fs = require('fs')
const path = require('path')

module.exports = function createFile(PROJECT_NAME, filename, filepath, data) {
  // make the file directory
  const pfolder = path.join(PROJECT_NAME, filepath)

  if( !fs.existsSync(pfolder) ) {
    fs.mkdirSync(pfolder, { recursive: true }, (err) => {
      if (err) throw err;
    });
  }

  const pfile = path.join(pfolder, filename)
  // make the file
  fs.writeFileSync(pfile, data, (err) => {
    if (err) throw err;
  });
}