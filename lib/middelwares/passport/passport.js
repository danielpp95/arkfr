const fs = require('fs')
const path = require('path')

const files = require('./files')
const createFile = require('../helpers')

module.exports = (PROJECT_NAME) => {
  files.forEach(file => {
    const f = path.join(__dirname, file.path, file.filename)
    const data = require(f)
    
    createFile(PROJECT_NAME, file.filename, file.path, data );
  });
}
