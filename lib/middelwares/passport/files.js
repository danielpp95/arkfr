module.exports = [
  {path: 'src/api/controllers/usercontroller/', filename: 'delete.ts'},
  {path: 'src/api/controllers/usercontroller/', filename: 'get.ts'},
  {path: 'src/api/controllers/usercontroller/', filename: 'logout.ts'},
  {path: 'src/api/controllers/usercontroller/', filename: 'signin.ts'},
  {path: 'src/api/controllers/usercontroller/', filename: 'signup.ts'},
  {path: 'src/api/controllers/usercontroller/', filename: 'update.ts'},
  {path: 'src/api/interfaces/', filename: 'IUser.ts'},
  {path: 'src/api/models/', filename: 'User.ts'},
  {path: 'src/config/', filename: 'passport.ts'}
]