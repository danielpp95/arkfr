module.exports = 
`const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
import User from '../api/models/User';

passport.use(new LocalStrategy({
  usernameField: 'email'
}, async (email: string, password: string, done: any) => {

  const user: any = await User.findOne({email});
  
  !user ? 
    done(null, false, {message: 'Not User Found'})
  : 
    async function() {
      const match = await user.matchPassword(password);
      console.log(match)
      match ?
        done(null, user)
      :
        done(null, false, {message: 'Incorrect Password'})
    }();
}))

passport.serializeUser((user: any, done: any) => {
  done(null, user.id);
});

passport.deserializeUser((id: string, done: any) => {
  User.findById(id, (err: any, user: any) => {
    done(err, user);
  });
});`