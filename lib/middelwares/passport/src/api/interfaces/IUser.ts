module.exports = 
`import mongoose from "mongoose";

export default interface IUser extends mongoose.Document {
  name: String
  email: String,
  password: String,
  date: Date
};`