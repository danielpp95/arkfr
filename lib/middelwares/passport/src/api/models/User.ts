module.exports = 
`import {Schema, model} from 'mongoose';
import bcrypt from 'bcryptjs';
import IUser from "../interfaces/IUser";

const UserSchema = new Schema({
  firstname: {type: String, required: true},
  lastname: {type: String, required: true},
  email: {type: String, required: true},
  password: {type: String, required: true, minlength: 6},
  date: {type: Date, default: Date.now}
});

UserSchema.methods.encryptPassword = async (password:string) => {
  const salt = await bcrypt.genSalt(10);
  const hash = bcrypt.hash(password, salt);
  return hash;
}

UserSchema.methods.matchPassword = async function (password: string) {
  return await bcrypt.compare(password, this.password);
};

export default model<IUser>('User', UserSchema);`