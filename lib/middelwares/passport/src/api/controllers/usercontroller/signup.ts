module.exports = 
`import {Request, Response} from 'express';
import User from '../../models/User';

module.exports = {
  friendlyName: '',
  inputs: { },
  exits: {},
  fn: async function (req: Request , res: Response) {
    const successRedirect: string = '';
    const failureRedirect: string = '';

    const { firstname, lastname, email, password, confirm_password} = req.body;
    const errors = Validate(firstname, lastname, email, password, confirm_password);
  
    const mailExits = await User.findOne({email});
    mailExits && errors.push({text: 'This mail is currently registered'});
    
    (errors.length > 0 )? 
      failureRedirect.trim().length > 0 ? res.render(failureRedirect, {errors, firstname, lastname, email}) : res.send(errors)
    : 
      async function ()  {
        const newUser: any = new User({firstname, lastname, email, password});
        newUser.password = await newUser.encryptPassword(password);
        await newUser.save();
    
        successRedirect.trim().length > 0 ? res.render(successRedirect) : res.send(newUser);
      }();
  }
}

function Validate (firstname: string, lastname: string, email: string, password: string, confirm_password: string) {
  const errors = [];

  !firstname && errors.push({text: \`Name can't be empty\`});
  !lastname && errors.push({text: \`Lastname can't be empty\`});
  !email && errors.push({text: \`Email can't be empty\`});
  password != confirm_password && errors.push({text: 'Passwod do not match'});
  password.length < 4 && errors.push({text: 'Passwod must be at least 4 characters'});

  return errors;
}`