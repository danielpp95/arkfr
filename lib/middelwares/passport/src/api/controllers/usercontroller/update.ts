module.exports = 
`import {Request, Response} from 'express';
import User from '../../models/User';

module.exports = {
  friendlyName: '',
  inputs: { },
  exits: {},
  fn: async function (req: Request , res: Response) {
    const successRedirect: string = '';
    const failureRedirect: string = '';

    const { firstname, lastname, email, password } = req.body;
    const me = req.user.id;
    const id = req.params.id;

    if (me === id) {
      await User.findByIdAndUpdate(id, {firstname, lastname, email, password});
      successRedirect.trim().length > 0 ? res.render(successRedirect) : res.send('user updated');
    }else{
      failureRedirect.trim().length > 0 ? res.render(failureRedirect) : res.send('Error: 403 Forbidden');
    }
  }
}`