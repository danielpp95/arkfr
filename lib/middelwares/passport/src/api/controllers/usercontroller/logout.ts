module.exports = 
`import {Request, Response} from 'express';

module.exports = {
  friendlyName: '',
  inputs: { },
  exits: {},
  fn: async function (req: Request , res: Response) {
    req.logOut();
    res.redirect('/');
  }
}`