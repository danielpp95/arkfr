module.exports = 
`import {Request, Response} from 'express';
import User from '../../models/User';

module.exports = {
  friendlyName: '',
  inputs: { },
  exits: {},

  fn: async function (req: Request , res: Response) {
    const successRedirect: string = '';
    const failureRedirect: string = '';

    let me = req.user.id;
    let id = req.params.id;

    if (me === id) {
      await User.findByIdAndDelete(req.params.id);
      successRedirect.trim().length > 0 ? res.render(successRedirect) : res.send('user deleted');
    }else{
      failureRedirect.trim().length > 0 ? res.render(failureRedirect) : res.send('Error: 403 Forbidden');
    }

  }
}`