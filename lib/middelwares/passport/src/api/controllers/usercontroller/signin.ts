module.exports = 
`import passport from 'passport';

module.exports = {
  friendlyName: '',
  inputs: { },
  exits: {},
  fn: passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/error',
    failureFlash: false
  })
}
`