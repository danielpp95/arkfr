module.exports = 
`import {Request, Response} from 'express';
import User from '../../models/User';

module.exports = {
  friendlyName: '',
  inputs: { },
  exits: {},
  fn: async function (req: Request , res: Response) {
    const user = await User.findById(req.params.id);
    
    res.send(user);
  }
}`