module.exports = (PROJECT_NAME, VIEW_ENGINE, DATABASE, MIDDLEWARES) => {
  if (MIDDLEWARES.includes('passport')) {require('./passport/passport')(PROJECT_NAME, VIEW_ENGINE, DATABASE, MIDDLEWARES)}
}