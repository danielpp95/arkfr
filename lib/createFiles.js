const shell = require("shelljs");
module.exports = (PROJECT_NAME, VIEW_ENGINE, DATABASE) => {
  shell.touch(PROJECT_NAME + '/src/app.ts');
  shell.touch(PROJECT_NAME + '/src/server/routes.ts');
  shell.touch(PROJECT_NAME + '/.gitignore');
  shell.touch(PROJECT_NAME + '/nodemon.json');
  shell.touch(PROJECT_NAME + '/readme.md');
  shell.touch(PROJECT_NAME + '/tsconfig.json');
  shell.touch(PROJECT_NAME + '/docek-compose.yml');
};