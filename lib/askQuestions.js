const inquirer = require("inquirer");
module.exports = () => {
  const questions = [
    {
      type: "list",
      name: "VIEW_ENGINE",
      message: "choose a view engine",
      choices: ["handlebars"]
    },
    {
      type: "list",
      name: "DATABASE",
      message: "choose a DataBase system",
      choices: ["mongoose"]
    },
    {
      type: "checkbox",
      name: 'MIDDLEWARES',
      message: "choose middlewares to use",
      choices: ['morgan', 'passport'],
      default: []
    }
  ];
  return inquirer.prompt(questions);
};


